#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

/**
 * Lit une ligne du fichier correspondant au descripteur passé en argument jusqu'à lire un retour à la ligne ('\n') ou
 * avoir lu size caractères.
 *
 * @param fd descripteur de fichier ouvert en lecture
 * @param s buffer dans lequel les octets lus sont écrits
 * @param size nombre maximum d'octets à lire
 * @return le nombre d'octets effectivement lus
 */

int lireligne(int fd, char *s, int size) {
    int bytes_read = 0;
    char c;

    while (bytes_read < size) {
        int bytes = read(fd, &c, 1);

        s[bytes_read] = c;
        bytes_read++;

        if (c == '\n') {
            return bytes_read ;
        }
    }

    return bytes_read;
}

void main() {
    int fd = open("message.txt", O_RDONLY);

    char *ligne = malloc(sizeof(char) * 1024);

    int bytes_lus = lireligne(fd, ligne, 1024);

    printf("Nombre de caractères lus : %d\n", bytes_lus);
    printf("Contenu de la ligne : %s\n", ligne);

    free(ligne);
    close(fd);
}
