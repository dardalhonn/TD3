#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

int main(int argc, char *argv[]) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s file1 file2 ... fileN\n", argv[0]);
        return EXIT_FAILURE;
    }

    struct stat buf;
    off_t max_size = 0;
    char *biggest_file = NULL;

    for (int i = 2; i < argc; i++) {
        if (stat(argv[i], &buf) == -1) {
            perror(argv[i]);
            continue;
        }
        if (S_ISREG(buf.st_mode) && (max_size == -1 || buf.st_size > max_size)) {
            max_size = buf.st_size;
            biggest_file = argv[i];
        }
    }

    printf("The biggest file is: %s\n", biggest_file);
    return EXIT_SUCCESS;
}
