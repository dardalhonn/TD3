#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>



int main(int argc, char **argv){
int source_fd, dest_fd;
    char buffer;
    source_fd = open(argv[argc-2], O_RDONLY);
    if (errno == ENOENT){
               perror(argv[argc-2]);
                }
    dest_fd = open(argv[argc-1], O_WRONLY | O_CREAT | O_TRUNC, 0644);
    if (errno == ENOENT){
               perror(argv[argc-1]);
                }
    while (read(source_fd, &buffer, 1) == 1) {
        if (write(dest_fd, &buffer, 1) != 1) {
            close(source_fd);
            close(dest_fd);
            exit(EXIT_FAILURE);
        }
    }
    close(source_fd);
    close(dest_fd);
    return EXIT_SUCCESS;
}

